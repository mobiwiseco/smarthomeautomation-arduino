#include <SPI.h>
#include <boards.h>
#include <RBL_nRF8001.h>
#include <services.h> 
#include "DHT.h"
#include <Servo.h>

#define DATASET_DOOR_INDEX 0
#define DATASET_LIGHT_SALOON_INDEX 1
#define DATASET_LIGHT_KITCHEN_INDEX 2
#define DATASET_TEMPERATURE_INDEX 3
#define DATASET_HUMIDITY_INDEX 4
#define DATASET_TV_INDEX 5
#define DATASET_AIR_CONDITIONER_INDEX 6

#define PIN_SALOON_LIGHT 2
#define PIN_KITCHEN_LIGHT 3
#define PIN_HUMIDITY_TEMP 4
#define PIN_SERVO_DOOR 5


byte home_set[7] = {0};
int index = 0;
DHT dht;
Servo servo_door;
int door_pos = 1;

void setup()
{  
  ble_set_name("My Smart Home");
  ble_begin();
  Serial.begin(57600);
  pinMode(PIN_SALOON_LIGHT, OUTPUT); 
  pinMode(PIN_KITCHEN_LIGHT, OUTPUT); 
  dht.setup(PIN_HUMIDITY_TEMP);
  servo_door.attach(PIN_SERVO_DOOR);
}

void loop()
{
  
  /*****************************************/
  
  //CHECK AVAILABLE DATA ON THE PIPE
  
  /*If there is available data on this pipe
    it will check all data and update home sensor data set statuses.
    For example; if Kitchen light is in index 7 and current status off
    ble_read will check status and update index 7 to status on.
  */
  if ( ble_available() )
  {
    while ( ble_available() )
    {
      byte buffer = ble_read();
      if(buffer == '&')
        index += 1;
      else
        home_set[index] = buffer;
    }
  }
  
  //END IF CHECK AVAILABLE DATA ON THE PIPE
  
  /*****************************************/
  
  //CHECK SALOON LIGHT
  /*Check saloon light index from data set high if equals 2
  */
  if(home_set[DATASET_LIGHT_SALOON_INDEX] == 2)
    digitalWrite(PIN_SALOON_LIGHT, HIGH);
  else
    digitalWrite(PIN_SALOON_LIGHT, LOW);
   //END OF CHECKING SALOON LIGHT 
   
  /*****************************************/
    
  //CHECK KITCHEN LIGHT
  /*Check kitchen light index from data set high if equals 2
  */ 
  if(home_set[DATASET_LIGHT_KITCHEN_INDEX] == 2)
    digitalWrite(PIN_KITCHEN_LIGHT, HIGH);
  else
    digitalWrite(PIN_KITCHEN_LIGHT, LOW);
   //END OF CHECK KITCHEN LIGHT 
  
   /*****************************************/
   
   //GET TEMPERATURE AND HUMIDITY VALUES AND WRITE THEM TO THEIR INDEXES
   home_set[DATASET_TEMPERATURE_INDEX] = byte(dht.getTemperature());
   home_set[DATASET_HUMIDITY_INDEX] = byte(dht.getHumidity());
   
   /*****************************************/
   
   if(home_set[DATASET_DOOR_INDEX] == 2 && door_pos == 1){
     servo_door.write(90);
     door_pos = 90;
   }
   else if( home_set[DATASET_DOOR_INDEX] == 1 && door_pos == 90){
     servo_door.write(1);
     door_pos = 1;
   }
  
  //WRITE CURRENT DATA SET TO PIPE
  
  /*Write current updated dataset to ble pipe.
    End of the loop it will be send by ble_do_events() method
    and pipe will be cleaned.
  */  
  for(int i = 0 ; i<7 ; i++){  
    ble_write(home_set[i]);
  }
  Serial.println();
  
  //END OF WRITING CURRENT DATA SET TO PIPE
  
  /*****************************************/

  ble_do_events();
  index = 0;
  delay(300);
}

